##[1.3.0] - 2020-05-12
- Built against Test Executive v1.10.1
- Includes support for Parallel flow control type

##[1.2.9] - 2020-05-12
- Built against Test Executive v1.9.6

##[1.2.8] - 2020-05-12
- Improved the draw tree method to reduce execution time

##[1.2.7] - 2020-05-12
- Updated Test Step Dialog to handle pointers correctly

##[1.2.6] - 2020-05-11
- Added protected write accessors for dialog type overrides

##[1.2.5] - 2020-05-11
- Added protected read accessors for dialog type overrides

##[1.2.4] - 2020-05-11
- Built against Test Executive v1.9.5

##[1.2.3] - 2020-05-05
- Had to relink an application dependency

##[1.2.2] - 2020-05-05
- Built against Test Executive v1.9.4

##[1.2.0] - 2020-05-05
- Built against Test Executive v1.9.3
- Built against MVA Viewable v0.1.2
- Improved shipping example
- Includes ability to run a Mass Relink on files

##[1.1.2] - 2020-04-06
- Built against Test Executive v1.8.2
- Built against MVA Viewable v0.1.1
- Built against MVA Popup ViewManager v1.1.1
- Built against File Explorer Extension v2.0.1

##[1.1.1] - 2020-04-01
- Built against Test Executive v1.8.1

##[1.1.0] - 2020-03-31
- Built against Popup ViewManager v1.1.0

##[1.0.0] - 2020-03-26
- Built against MVA-Core dependencies

##[0.0.31] - 2020-03-19
- Built against Test Executive v1.8.0
- Added ability to Insert a Halt flow control step into a test executive

##[0.0.30] - 2020-03-18
- Built against Test Executive v1.7.1

##[0.0.29] - 2020-03-17
- Built against Test Executive v1.7.0
- Save As will now create new GUIDs for the new file

##[0.0.28] - 2020-01-15
- Added the ability to insert a Case Structure.
- This patch includes work in progress on creating basic Views that will make it easier to consume the package.

##[0.0.27] - 2019-10-31
- Fixed a bug that was prematurely saving freshly created test executives.  This bug was only visible if the Create Test Executive method was overridden, called the parent, and acted on the newly created Test Exec guid.

##[0.0.26] - 2019-10-30
- Built against Test Executive v1.5.0

##[0.0.25] - 2019-10-29
- Built against Test Executive v1.4.12

##[0.0.24] - 2019-10-29
- Built against Test Executive v1.4.11

##[0.0.23] - 2019-10-23
- Re-link and mass compile

##[0.0.22] - 2019-10-23
- Added alpha mask to prevent user from interacting with initial test executive

##[0.0.21] - 2019-10-23
- Built against Test Executive v1.4.5

##[0.0.20] - 2019-10-15
- Added accessors for injected object definitions

##[0.0.19] - 2019-10-11
- Building in ability to Save As and Create Parameterized on Test Executive files

##[0.0.18] - 2019-10-09
- Had to do some manual relinking of dependent packages

##[0.0.17] - 2019-10-08
- Built against File Explorer Extension v1.1.2

##[0.0.16] - 2019-10-07
- Refactored example application

##[0.0.15] - 2019-10-07
- Built against Test Executive v1.4.2

##[0.0.13] - 2019-09-30
- Added move/copy row highlighting to make it easier for users to tell where they are going to move/copy an element to

##[0.0.12] - 2019-09-18
- Added ability to provide custom symbols to test executive view

##[0.0.11] - 2019-09-16
- Built against File Explorer Extension v1.1.0

##[0.0.10] - 2019-09-15
- Built against Test Executive v1.3.0

##[0.0.9] - 2019-09-13
- Built against Test Executive v1.2.1
- Built against MVA v2.2.3

##[0.0.7] - 2019-09-05
- Built against Test Executive v1.0.0

##[0.0.6] - 2019-08-20
- Built against Test Executive v0.0.46-Beta

##[0.0.5] - 2019-08-20
- Built against Test Executive v0.0.45-Beta

##[0.0.4] - 2019-08-20
- Built against Test Executive v0.0.44-Beta

##[0.0.3] - 2019-08-20
- GPM is failing to recognize v0.0.2 as an existing package for some reason so I am going to try publishing again as v0.0.3
- There are no changes to source as compared to v0.0.2

##[0.0.2] - 2019-08-20
- Built against Test Executive v0.0.43-Beta

##[0.0.1] - 2019-08-10
- Initial commit